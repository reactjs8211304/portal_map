import axios from "axios"

export const getProvinceJson = async () => {
  let data = axios.get("/provinces.json")
  return data
}