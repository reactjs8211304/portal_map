import VectorSource from 'ol/source/Vector'
import Polygon from 'ol/geom/Polygon'
import Feature from 'ol/Feature'
import { Style, Stroke, Fill } from 'ol/style';
import VectorLayer from 'ol/layer/Vector'
import { fromLonLat } from 'ol/proj';
import OSM from 'ol/source/OSM';
import TitleLayer from 'ol/layer/Tile'
import XYZ from 'ol/source/XYZ'
import { Point } from 'ol/geom';
import Select from 'ol/interaction/Select';
import { click } from 'ol/events/condition';

export const addPolygon = (geoJson, style) => {
  let coordinates = JSON.parse(geoJson.geojson);
  coordinates = coordinates.coordinates[0][0]
  // convert sang he toa do EPSG:3857 
  coordinates = coordinates.map((r) => {
    if (r) return fromLonLat(r)
  })
  let vectorLayer = new VectorLayer({
    source: new VectorSource({
      features: [
        new Feature({
          geometry: new Polygon([
            coordinates
          ]),
        })
      ]
    }),
    zIndex: 10,
    style,
  });
  vectorLayer.on('click', (e) => { console.log(e) })
  return vectorLayer
}

export const addMarker = (location, style) => {
  let markerSource = new VectorSource();
  let markerLayer = new VectorLayer({
    source: markerSource,
    style,
    zIndex: 2
  });
  let marker = new Feature({
    geometry: new Point(fromLonLat(location))
  });

  markerSource.addFeature(marker)

  return markerLayer
}

export const getStylePolygon = ({ color, fill, opacity = 1 }) => {
  return new Style({
    stroke: new Stroke({
      color: color,
      width: 3,
    }),
    fill: new Fill({
      color: fill.replace(")", `, ${opacity})`),
    }),
  })
}

export const tileArcgisMapLayer = () => {
  return new TitleLayer({
    source: new XYZ({
      url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer/tile/{z}/{y}/{x}'
    }),
    zIndex: 0
  })
}

export const tileOsmLayer = () => {
  return new TitleLayer({
    source: new OSM({
      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    }),
    zIndex: 0
  })
}

export const selectPolygon = (vectorLayer, clickAction) => {
  let select = new Select({
    layers: [vectorLayer],
    condition: click,
    hitTolerance: 0 // dung sai click
  });
  select.on('select', () => clickAction());
  return select;
}