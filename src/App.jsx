import React, { useState } from "react";
import MapComponent from "./components/map";
import MenuRight from "./components/menuRight";
const App = () => {
  const [typeMap, setTypeMap] = useState(1);
  const [checkPoint, setCheckPoint] = useState(true);
  const [checkPolygon, setCheckPolygon] = useState(true);
  return (
    <>
      <h2
        style={{
          textAlign: "center",
          fontWeight: "bold",
          background: "#61A5EB",
          padding: "10px 0",
          color: "#fff",
        }}
      >
        Cổng thông tin không gian địa lý
      </h2>
      <MapComponent
        typeMap={typeMap}
        checkPoint={checkPoint}
        checkPolygon={checkPolygon}
      />
      <MenuRight
        typeMap={setTypeMap}
        checkPoint={setCheckPoint}
        checkPolygon={setCheckPolygon}
      />
    </>
  );
};

export default App;
