import React, { useState, useEffect, useRef, memo } from 'react'
import Map from 'ol/Map'
import View from 'ol/View'
import { fromLonLat } from 'ol/proj';
import { Group as LayerGroup } from 'ol/layer.js';
import { getProvinceJson } from "../services/polygon-services"
import { polygonColor, center } from "../utils/data"
import {
  addPolygon,
  getStylePolygon,
  tileArcgisMapLayer,
  tileOsmLayer,
  addMarker,
  selectPolygon
} from "../utils/function"
import DotIcon from "/dot.svg"
import { Style, Icon } from 'ol/style';
import { listPoint } from "../utils/data"

const map = ({ typeMap, checkPoint, checkPolygon }) => {
  const mapContainer = useRef(null);
  const [map, setMap] = useState(null);
  const [arcgisLayer, setArcgisLayer] = useState(null);
  const [osmLayer, setOsmLayer] = useState(null);
  const [groupLayerPolygon, setGroupLayerPolygon] = useState(null);
  const [groupLayerPoint, setGroupLayerPoint] = useState(null);
  const [checkPointSwitch, setCheckPointSwitch] = useState(true);
  const [checkPolygonSwitch, setCheckPolygonSwitch] = useState(true);

  useEffect(() => {
    const initMap = new Map({
      target: mapContainer.current,
      layers: [],
      view: new View({
        center: fromLonLat(center),
        zoom: 8
      })
    })
    setMap(initMap)
  }, [])

  useEffect(() => {
    if (!map || !groupLayerPoint) return
    if (checkPointSwitch !== checkPoint) {
      if (checkPoint) {
        map.addLayer(groupLayerPoint)
      } else {
        map.removeLayer(groupLayerPoint)
      }
      setCheckPointSwitch(checkPoint)
    }
  }, [checkPoint, map, groupLayerPoint])

  useEffect(() => {
    if (!map || !groupLayerPolygon) return
    if (checkPolygonSwitch !== checkPolygon) {
      if (checkPolygon) {
        map.addLayer(groupLayerPolygon)
      } else {
        map.removeLayer(groupLayerPolygon)
      }
      setCheckPolygonSwitch(checkPolygon)
    }
  }, [checkPolygon, map, groupLayerPolygon])

  useEffect(() => {
    if (map) {
      const arcgisMapServerLayer = tileArcgisMapLayer()
      const osmMapLayer = tileOsmLayer()
      setArcgisLayer(arcgisMapServerLayer)
      setOsmLayer(osmMapLayer)
      if (arcgisLayer) map.removeLayer(arcgisLayer)
      if (osmLayer) map.removeLayer(osmLayer)
      if (typeMap === 1) map.addLayer(arcgisMapServerLayer)
      if (typeMap === 2) map.addLayer(osmMapLayer)
    }
  }, [map, typeMap])

  useEffect(() => {
    const fetchData = async () => {
      if (groupLayerPolygon) map.removeLayer(groupLayerPolygon)
      let response = await getProvinceJson();
      response = response?.data;
      if (!response) return;
      let groupLayerPolygonMap = new LayerGroup({
        layers: [],
        name: 'polyGroup'
      })
      response.map((r, i) => {
        if (i < 10) {
          let style = getStylePolygon({
            color: polygonColor[i % 3],
            fill: polygonColor[i % 3],
            opacity: 0.2
          })
          let proPoly = addPolygon(r, style)
          let select = selectPolygon(proPoly, ()=>{
            console.log('select')
          });
          map.addInteraction(select)
          groupLayerPolygonMap.getLayers().push(proPoly)
        }
      })
      map.addLayer(groupLayerPolygonMap)
      setGroupLayerPolygon(groupLayerPolygonMap)
    }
    if (map) fetchData()
  }, [map])

  useEffect(() => {
    if (map) {
      if (groupLayerPoint) map.removeLayer(groupLayerPoint)
      let groupLayerMarker = new LayerGroup({
        layers: [],
        name: 'markerGroup'
      })
      var markerStyle = new Style({
        image: new Icon({
          src: DotIcon,
        })
      });
      listPoint.map((p) => {
        let marker = addMarker(p, markerStyle)
        groupLayerMarker.getLayers().push(marker)
      })
      map.addLayer(groupLayerMarker)
      setGroupLayerPoint(groupLayerMarker)
    }
  }, [map])

  return (
    <div ref={mapContainer} className="map-container" />
  )
}

export default memo(map)