import React, { useState, useEffect, memo } from 'react'
import { Switch } from 'antd';

const MenuRight = (props) => {
  const [checkArcgis, setCheckArcgis] = useState(true)
  const [checkOsm, setCheckOsm] = useState(false)

  useEffect(() => {
    if (checkArcgis) props?.typeMap(1)
    if (checkOsm) props?.typeMap(2)
  }, [checkArcgis, checkOsm])

  return (
    <div className='layout-menu-action'>
      <Switch
        checkedChildren="ArcGis MapServer"
        unCheckedChildren="ArcGis MapServer"
        checked={checkArcgis}
        style={{ width: 140, marginTop: 10 }}
        onChange={(e) => {
          setCheckArcgis(e)
          setCheckOsm(!e)
          props?.typeMap(1)
        }}
      />
      <Switch
        checkedChildren="Open Street Map"
        unCheckedChildren="Open Street Map"
        style={{ width: 140, marginTop: 10 }}
        checked={checkOsm}
        onChange={(e) => {
          setCheckOsm(e)
          setCheckArcgis(!e)
          props?.typeMap(2)
        }}
      />
      <Switch
        checkedChildren="Điểm toạ độ"
        unCheckedChildren="Điểm toạ độ"
        defaultChecked
        style={{ width: 140, marginTop: 10 }}
        onChange={(e) => props?.checkPoint(e)}
      />
      <Switch
        checkedChildren="Vùng màu"
        unCheckedChildren="Vùng màu"
        defaultChecked
        style={{ width: 140, marginTop: 10, marginBottom: 10 }}
        onChange={(e) => props?.checkPolygon(e)}
      />
    </div>
  )
}

export default memo(MenuRight)
