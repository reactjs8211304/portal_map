import React, { useRef, useState, useEffect } from 'react';
import {
  geoJSON,
  Map,
} from 'leaflet';
import RouterWarning from "../../../../images/RouterWarning.svg"
import RouterNomarl from "../../../../images/Router.svg"
import MordemNomarl from "../../../../images/Mordem.svg"
import PCWarning from "../../../../images/PCWarning.svg"
import PCNomarl from "../../../../images/PC.svg"
import FireWallWarning from "../../../../images/FireWallWarning.svg"
import FireWallNomarl from "../../../../images/FireWall.svg"
import ServerWarning from "../../../../images/ServerWarning.svg"
import ServerNomarl from "../../../../images/Server.svg"
import SwitchWarning from "../../../../images/SwitchWarning.svg"
import StationWarning from "../../../../images/station1Warning.svg"
import SwitchNomarl from "../../../../images/Switch.svg"
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';
import {
  routerDirection,
  getHeadQuarterByLocation,
  getMapLine,
  getUnitColor,
  getProvinceTiles,
  getDistrictsTiles,
  getCommunesTiles,
  getUnitByArea
} from '../../services';
import { polygonColor } from '../../const/colors';
import polyline from "@mapbox/polyline";
import ReactDOMServer from "react-dom/server";
import PopupMarker from "../Marker/popupMarker";
import Station1 from "../../../../images/station1.svg"
import Station2 from "../../../../images/station2.svg"
import StationProvince1 from "../../../../images/StationProvince1.svg"
import StationProvince2 from "../../../../images/StationProvince2.svg"
import StationProvince1Warning from "../../../../images/StationProvince1Warning.svg"
import { APP_URL_JSON_FILE, LY_SON_LOCALTION } from "../../../../utils/constants";

import provinces from '../../../../../static/provinces.json';
import districts from '../../../../../static/districts.json';
import wards from '../../../../../static/communes.json';

export const MapComponent = (props) => {
  // fix
  const [unitColor, setUnitColor] = useState();
  const [colorOk, setColorOk] = useState('#05eb05');
  const [colorError, setColorError] = useState('#FA2124');
  const [colorOverload, setColorOverload] = useState('#FCB048');
  const [Provinces, setProvinces] = useState(null);
  const [Districts, setDistricts] = useState(null);
  const [Communes, setCommunes] = useState(null);
  const [load, setLoad] = useState(true)

  useEffect(() => {
    const fetchData = async () => {
      let rs = await getMapLine()
      const tempData = rs.data;
      if (!tempData) return;

      //20220818 Fix
      const okData = tempData.find(x => x.code == '01'); if (okData) setColorOk(okData.color);
      const overLoadData = tempData.find(x => x.code == '02'); if (overLoadData) setColorOverload(overLoadData.color);
      const errorData = tempData.find(x => x.code == '03'); if (errorData) setColorError(errorData.color);
      //20220818 end
    }
    fetchData()
  }, [])

  useEffect(() => {
    const fetchData = async () => {
      // setProvinces(await getProvinceTiles());
      // setDistricts(await getDistrictsTiles());
      // setCommunes(await getCommunesTiles());

      setProvinces(provinces);
      setDistricts(districts);
      setCommunes(wards);

      let rs = await getUnitColor()
      if (rs.data) setUnitColor(rs.data)
    }
    fetchData()
  }, [])

  const containerMap = useRef(null);
  const [map, setMap] = useState(null);
  const [networkLayer, setNetworkLayer] = useState(null);
  const [markerClusterLayer, setMarkerClusterLayer] = useState(null);
  const [markerCluster, setMarkerCluster] = useState(null);
  const [networkCluster, setNetworkCluster] = useState(null);
  const [mainBounds, setMainBounds] = useState({});
  const [nowBounds, setNowBounds] = useState({});
  const [mapLayer, setMapLayer] = useState(null);

  useEffect(() => {
    if (map && markerCluster) {
      if (markerClusterLayer) {
        map.removeLayer(markerClusterLayer);
      }
      var markers = L.markerClusterGroup({
        maxClusterRadius: 50,
        iconCreateFunction: function (cluster) {
          var markers = cluster.getAllChildMarkers().sort((item1, item2) => {
            if (item1.options.info.type === "error") {
              return 1;
            }
            else return -1;
          });
          markers = markers.reverse()
          return markers.length > 0 ? markers[0].getIcon() : L.divIcon({ iconSize: L.point(0, 0) });
        },
        spiderfyOnMaxZoom: false, showCoverageOnHover: false, zoomToBoundsOnClick: true
      });
      markerCluster.map(async r => {
        if (!r.latitude || !r.longitude) return;
        const dataDevice = await getHeadQuarterByLocation(r.id)
        let newDataDeivice = []
        if (r.sourceLink) {
          r.sourceLink.map(sl => {
            let de = dataDevice.find(find => find.ipAddress === sl)
            /* 20220818 fix */
            if (de) {
              const dT = r.devices.find(d => d.sourceLink.indexOf(de.ipAddress) >= 0);
              return newDataDeivice.push({ ...de, alertStatus: dT ? (dT.alertStatus || 'DeviceUp') : 'DeviceUp' });
            }
            /* 20220818 fix end */
            return
          })
        }
        var mk = L.marker([r.latitude, r.longitude], {
          info: r,
          icon: L.icon({
            iconUrl: getIconByType({}, r),
            iconSize: [36, 36],
            iconAnchor: [18, 18],
            popupAnchor: [0, -18]
          }),
        })
          .bindPopup(
            ReactDOMServer.renderToStaticMarkup(
              <PopupMarker
                r={r}
                dataDevice={newDataDeivice}
                setOpenAdd={props.setOpenAdd}
                setDataDevice2={props.setDataDevice2}
              />
            ),
            { className: `width-popup device-popup${r.type == "error" ? " error" : ""}`, closeOnClick: true, closeButton: false, closeOnEscapeKey: true },
          );
          // tooltip icon
          // mk.bindTooltip(
          //   ReactDOMServer.renderToStaticMarkup(
          //   <PopupMarker
          //     r={r}
          //     dataDevice={newDataDeivice}
          //     setOpenAdd={props.setOpenAdd}
          //     setDataDevice2={props.setDataDevice2}
          //   />
          // ),
          // ),{
          //   sticky: true,
          //   interactive: true,
          //   opacity: 0.6,
          //   className: 'leaflet-tooltip-own',
          // }
          //
        markers.addLayer(mk);
        // if (r.alertStatus) mk.openPopup();
      });
      map.addLayer(markers);
      setMarkerClusterLayer(markers);
    }
  }, [markerCluster]);

  useEffect(() => {
    if (containerMap && !map)
      setMap(
        new Map(containerMap.current, {
          width: '100%',
          height: '100%',
          minZoom: 5,
          maxZoom: 18,
          zoom: 5,
          renderer: L.canvas(),
          center: [20.585118650779936, 105.9070274509489],
          zoomControl: false,
          wheelPxPerZoomLevel: 300,
        }),
      );
  }, [containerMap]);

  const getIconByType = (r, e) => {
    switch (r.deviceTypeId) {
      case 1: {
        return (e.type == "error" && r.ipAddress == e.sourceLink[0]) ? RouterWarning : RouterNomarl;
      }
      case 3: {
        return (e.type === "error" && r.ipAddress == e.sourceLink[0]) ? SwitchWarning : SwitchNomarl;
      }
      case 6: {
        return (e.type === "error" && r.ipAddress == e.sourceLink[0]) ? PCWarning : PCNomarl;
      }
      case 5: {
        return (e.type === "error" && r.ipAddress == e.sourceLink[0]) ? MordemNomarlWarning : MordemNomarl;
      }
      case 4: {
        return (e.type === "error" && r.ipAddress == e.sourceLink[0]) ? FireWallWarning : FireWallNomarl;
      }
      case 2: {
        return (e.type == "error" && r.ipAddress == e.sourceLink[0]) ? ServerWarning : ServerNomarl;
      }
      default: return e.type == "error" ?
        props.typeStation === "country" ? StationWarning : StationProvince1Warning :
        (e.active ?
          props.typeStation === "country" ? Station2 : StationProvince2 :
          props.typeStation === "country" ? Station1 : StationProvince1
        );
    }
  }

  const lineWidth = (r) => {
    // 20220818 fix
    if (!r) return 20;
    // 20220818 end

    switch (r.linkLevel) {
      case 0: {
        return 45;
      }
      case 1: {
        return 35;
      }
      case 2: {
        return 25;
      }
      default: return 20;
    }
  }

  const initMap = () => {
    if (map && Provinces && Communes && Districts) {
      L.marker([16.351613, 112.7427], {
        icon: L.divIcon({
          html: '<span>Quần đảo Hoàng Sa</span>',
          iconSize: [150, 30],
          iconAnchor: [75, 0],
          opacity: 0.8,
        }),
      }).addTo(map);
      L.marker([9.51484, 114.68724], {
        icon: L.divIcon({
          html: '<span>Quần đảo Trường Sa</span>',
          iconSize: [160, 30],
          iconAnchor: [80, 0],
          opacity: 0.8,
        }),
      }).addTo(map);
      if (mapLayer) map.removeLayer(mapLayer);
      if (networkLayer) map.removeLayer(networkLayer);

      const proLayer = new L.featureGroup({}).addTo(map);
      setNetworkLayer(new L.featureGroup({}).addTo(map));
      (async () => {
        const list = [];
        let Pros;
        if (props.provinceId != null && props.districtId != null) {
          Pros = Communes.filter(dist => dist.district_id == props.districtId);
        }
        else if (props.provinceId != null) {
          Pros = Districts.filter(dist => dist.province_id == props.provinceId);
        }
        else Pros = Provinces;

        Pros.map((pro, index) => {
          let boundId;
          let currentUnitColor;//fix
          if (props.provinceId != null && props.districtId != null) {
            boundId = pro.commune_id;
          }
          else if (props.provinceId != null) {
            boundId = pro.district_id;
          }
          else {
            boundId = pro.province_id;
          }
          currentUnitColor = unitColor && unitColor !== undefined && unitColor.find(x => Number(x.boundId) == boundId)  //fix
          list.push(pro);
          const geo = geoJSON(JSON.parse(pro.geojson), {
            style: feature => ({
              // fillColor: polygonColor[boundId % 5],
              // fillColor: currentUnitColor !== undefined ? currentUnitColor.color : polygonColor[boundId % 5], //fix
              fillColor: currentUnitColor !== undefined && currentUnitColor.color ? currentUnitColor.color : polygonColor[boundId % 5], //fix
              weight: 2,
              color: '#18416B',
              dashArray: 0,
              opacity: 1,
              fillOpacity: 0.5,
            }),
            onEachFeature(feature, layer) {
              layer.on({
                contextmenu: e => {
                  // Hard code for lock province missing districts
                  if (props.provinceId != null && [104, 217, 305, 411, 503, 711, 801, 101, 701, 501].indexOf(pro.province_id) == -1) return;
                  // End

                  if (props.viewChild) props.viewChild(boundId);
                },
                mouseover: e => {
                  const layer = e.target;
                  layer.setStyle({
                    weight: 5,
                    dashArray: '',
                    fillOpacity: 0.2,
                  });
                },
                mouseout: e => {
                  geo.resetStyle(e.target);
                },
                click: e => {
                  map.fitBounds(e.target.getBounds());
                }
              });
            },
          });
          geo.bindTooltip(pro.commune_full || (pro.district_full || pro.province_full), {
            sticky: true,
            interactive: true,
            opacity: 0.6,
            className: 'leaflet-tooltip-own',
          });
          geo.addTo(proLayer);
        });

        setMainBounds(proLayer.getBounds());
        map.fitBounds(proLayer.getBounds(), { paddingTopLeft: [!props.provinceId && !props.districtId && !props.communeId ? 350 : 0, 0] });
        setMapLayer(proLayer);
      })();
    }
  }

  useEffect(() => {
    if (mapLayer && map) {
      if (props.colorAreaStatus) map.addLayer(mapLayer);
      else map.removeLayer(mapLayer);
    }
  }, [props.colorAreaStatus])

  useEffect(() => {
    initMap();
  }, [props.provinceId, props.districtId, unitColor, Provinces, Communes, Districts]);

  useEffect(() => {
    if (map) {
      L.mapboxGL({
        style: `${APP_URL_JSON_FILE}/style.json`
      }).addTo(map);
    }
    initMap();
  }, [map]);

  const goToBounds = ({ province, district, commune }) => {
    if (map && Provinces && Communes && Districts) {
      let geo;
      if (commune) {
        let communeFind = Communes.find(c => c.commune_id == commune)
        geo = communeFind ? geoJSON(JSON.parse(communeFind.geojson)) : ''
      }
      else if (district) {
        let districtFind = Districts.find(c => c.district_id == district)
        geo = districtFind ? geoJSON(JSON.parse(districtFind.geojson)) : geoJSON(JSON.parse(Provinces.find(c => c.province_id == province).geojson))
      }
      else if (province) {
        geo = geoJSON(JSON.parse(Provinces.find(c => c.province_id == province).geojson))
      }
      map.fitBounds(geo ? geo.getBounds() : mainBounds);
    }
  }

  useEffect(() => {
    if (
      // props.bounds &&
      // JSON.stringify(props.bounds) != JSON.stringify(nowBounds) &&
      // (
      props.bounds && (
        props.bounds.province ||
        props.bounds.district ||
        props.bounds.commune
      )
    ) {
      setNowBounds(props.bounds);
      goToBounds(props.bounds);
    }
  }, [props.bounds, Provinces, Communes, Districts]);

  useEffect(() => {
    if (
      !props.bounds.province &&
      !props.bounds.district &&
      !props.bounds.commune &&
      !props.unitIdArea
    ) {
      initMap()
    }
  }, [props.bounds, props.unitIdArea])

  useEffect(() => {
    const fetchData = async () => {
      if (props.unitIdArea) {
        const rs = await getUnitByArea(props.unitIdArea);
        if (!rs) return;
        let coordinates = []
        rs.data.map(r => {
          let arrCoordinates = Provinces.find(c => c.province_id == r)
          arrCoordinates = JSON.parse(arrCoordinates.geojson).coordinates[0][0]
          coordinates = [...coordinates, ...arrCoordinates]
        })
        const object = {
          type: "MultiPolygon",
          coordinates: [[coordinates]]
        }
        let geo = geoJSON(object);
        map.fitBounds(geo ? geo.getBounds() : mainBounds);
      }
    }
    fetchData()
  }, [props.unitIdArea])

  useEffect(() => {
    const fetchData = async () => {
      if (networkLayer && props.netWorkDevide.networkLinks) {
        networkLayer.clearLayers();
        if (props.toggleLine) {
          let temp = networkCluster.filter(r => {
            if (!props.lineTW_TW && r.linkLevel == 0) return false;
            if (!props.lineCity_Province && r.linkLevel == 2) return false;
            if (!props.lineProvince_Ward && r.linkLevel == 3) return false;
            if (!props.lineTW_City && r.linkLevel == 1) return false;
            if (!props.lineBackup && r.linkType == 1) return false;
            return true;
          });

          temp = await Promise.all(temp.map(async r => {
            try {
              const endDevice = r.manageUnits;
              // -------- nhu luc dau
              if (endDevice) {
                if (endDevice[0] && endDevice[1]) {
                  const start_point = `${endDevice[0].longitude},${endDevice[0].latitude}`
                  const end_point = `${endDevice[1].longitude},${endDevice[1].latitude}`

                  const rou = end_point != "null,null" && start_point != "null,null" &&
                    end_point != LY_SON_LOCALTION   // fix tam dao ly son quang ngai
                    && await routerDirection(`${end_point};${start_point}`) // fix tam

                  const dataLine = rou && polyline.decode(rou.data.routes[0].geometry)
                  return {
                    ...r,
                    dataLine
                  }
                }
              }
            }
            catch (_) {
              return null;
            }

            //---------------------------------------------------------------------------------  tra ra bao nhieu diem thi ve bay nhieu duong
            // if (endDevice.length > 0) {
            //   let listArrPoit = []
            //   endDevice.map(e => {
            //     if (e.latitude && e.longitude && e.latitude !== "null,null" && e.longitude !== "null,null") {
            //       return listArrPoit = [...listArrPoit, `${ e.longitude }, ${ e.latitude }`]
            //     }
            //   })
            //   if (listArrPoit && listArrPoit.length > 1) {
            //     const rou = await routerDirection(listArrPoit.join(";"))
            //     const dataLine = rou && polyline.decode(rou.data.routes[0].geometry)
            //     return {
            //       ...r,
            //       dataLine
            //     }
            //   }
            // }
          }));
          temp = temp.filter(t => t != null);

          temp.sort((a, b) => {
            let aVal = 0;
            let bVal = 0;
            if (a.type == "error") aVal = 2;
            if (b.type == "error") bVal = 2;
            if (a.linkType == 1) aVal = 1;
            if (b.linkType == 1) bVal = 1;

            return aVal - bVal;
          }).map(r => {
            //20220818 trick for r is null;
            if (!r) r = {};
            //20220818 End

            let getLineWidth = lineWidth(r) / (r.linkType == 1 ? 1.2 : 1);
            let color = colorOk; // fix
            const typeByte = r.bandwidth && r.bandwidth.split(/\s/)[1].toLowerCase()
            let bw = r.bandwidth && parseInt(r.bandwidth.split(/\s/)[0]);
            switch (typeByte) {
              case "gbps":
                bw = bw * 1000 * 1000 * 1000
                break;
              case "mbps":
                bw = bw * 1000 * 1000
                break;
              case "kbps":
                bw = bw * 1000
                break;
              default:
                break;
            }

            const metric = r.metricsQuantity * 8 * 100 / bw
            // const metric = r.metricsQuantity * 8 * 100 * (Math.floor(Math.random() * 34) + 20) / (bw * 1024 * 1024);
            if (metric > 60) {
              color = colorOverload; // fix
            }
            else if (metric >= 95) {
              color = colorError; // fix
            }

            L.polyline(r.dataLine, {
              color: "#FFF",
              weight: getLineWidth / 5
            }).addTo(networkLayer)
            const line = L.polyline(r.dataLine, {
              color: r.type == "error" ? colorError : color, // fix
              dashArray: r.linkType == 1 ? `35 10 ${parseInt((getLineWidth - 12) / 5)} 10` : (
                r.type == "error" ? "35 10" : 0
              ),
              weight: (getLineWidth - 12) / 5,
            }).bindPopup(
              ` <ul class="js-tabs__content-container">
                  <li class="js-tabs__content active">
                    <p>
                      <b>Tốc độ giới hạn đường kết nối:</b> ${r.cappedSpeed}
                      <br />
                      <b>Băng thông giới hạn:</b> ${r.bandwidth}
                      <br />
                      <b>Băng thông hiện tại:</b> ${r.metricsQuantity || ''}
                      <br />
                      <b>Loại đường kết nối:</b> ${r.linkTypeName || ''}
                      <br />
                      <b>Cấp độ đường kết nối:</b> ${r.linkLevelName || ''}
                    ${r.alertStatus ? `<br/><b>Sự cố:</b> ${r.alertStatus} - ${r.alertLevel} - ${r.alertTime}` : ''
              }
                    </p>
                  </li>
              </ul> `,
              { className: `device - popup${r.type == "error" ? " error" : ""} `, closeOnClick: true, closeButton: false, closeOnEscapeKey: true }
            );
            line.addTo(networkLayer)
          })
        }
      }
    }
    fetchData()
  }, [
    networkCluster, networkLayer, props.toggleLine,
    props.lineTW_TW, props.lineCity_Province, props.lineProvince_Ward,
    props.lineTW_City, props.lineBackup
  ])

  useEffect(() => {
    if (props.dataSocket && !load) {
      // 20220818 Fix debounce update State
      if (!window.tempSocketData) window.tempSocketData = [];
      window.tempSocketData = [...window.tempSocketData, ...cloneDeep(props.dataSocket)];
      debounceUpdateState();
      // 20220818 Fix end
    }
  }, [props.dataSocket, load])

  // 20220818 Fix debounce update State
  const debounceUpdateState = () => {
    if (!window.tempSocketData) return;
    if (window.timeoutUpdate) clearTimeout(window.timeoutUpdate);
    window.timeoutUpdate = setTimeout(_ => {
      let tempNetworks = [...(networkCluster || [])];
      let tempMarkers = [...(markerCluster || [])];
      cloneDeep(window.tempSocketData).map(dataSocket => {
        if (dataSocket.type == 11 || dataSocket.type == 12 || dataSocket.type == 100) {
          tempNetworks = tempNetworks.map(n => {
            const arrLat = n.manageUnits.map(r => r.latitude)
            const arrLng = n.manageUnits.map(r => r.longitude)
            let arr = [...arrLat, ...arrLng]
            if (
              dataSocket.sourceLink &&
              arr.includes(dataSocket.latitude[0]) &&
              arr.includes(dataSocket.latitude[1]) &&
              arr.includes(dataSocket.longitude[0]) &&
              arr.includes(dataSocket.longitude[1])
            ) return {
              ...dataSocket,
              ...n,
              type: dataSocket.type == 100 ? "Bw" : ((dataSocket.alertStatus !== "DeviceUp" && dataSocket.alertStatus !== "LinkOk") ? "error" : dataSocket.alertStatus)
            }
            return n;
          });
        }
        else {
          tempMarkers = tempMarkers.map(m => {
            // if (dataSocket.sourceLink && dataSocket.sourceLink.indexOf(m.ipAddress) >= 0) return {
            /* 20220818 fix */
            if (m.ipAddresses.indexOf(dataSocket.sourceLink[0]) >= 0) {
              let deviceErrors = m.deviceErrors || [];
              let devices = m.devices || [];
              if (!devices.find(d => d.sourceLink[0] == dataSocket.sourceLink[0])) {
                devices.push(dataSocket);
              }
              if (deviceErrors.indexOf(dataSocket.sourceLink[0]) >= 0 && dataSocket.alertStatus === "DeviceUp") {
                deviceErrors = deviceErrors.filter(i => i != dataSocket.sourceLink[0]);
              }
              if (deviceErrors.indexOf(dataSocket.sourceLink[0]) >= 0 && dataSocket.alertStatus === "InterfaceAdminUp") {
                deviceErrors = deviceErrors.filter(i => i != dataSocket.sourceLink[0]);
              }
              if (deviceErrors.indexOf(dataSocket.sourceLink[0]) >= 0 && dataSocket.alertStatus === "InterfaceUp") {
                deviceErrors = deviceErrors.filter(i => i != dataSocket.sourceLink[0]);
              }
              if (deviceErrors.indexOf(dataSocket.sourceLink[0]) < 0 && dataSocket.alertStatus != "DeviceUp"
                && dataSocket.alertStatus != "InterfaceAdminUp" && dataSocket.alertStatus != "InterfaceUp") {
                deviceErrors = [...deviceErrors, dataSocket.sourceLink[0]]
              }
              // if (deviceErrors.indexOf(dataSocket.sourceLink[0]) < 0 && dataSocket.alertStatus != "InterfaceAdminUp") {
              //   deviceErrors = [...deviceErrors, dataSocket.sourceLink[0]]
              // }
              // if (deviceErrors.indexOf(dataSocket.sourceLink[0]) < 0 && dataSocket.alertStatus != "InterfaceUp") {
              //   deviceErrors = [...deviceErrors, dataSocket.sourceLink[0]]
              // }
              return {
                ...dataSocket,
                ...m,
                alertStatus: dataSocket.alertStatus,
                devices: devices,
                // fix nhieu thiet bi tai 1 tru so, dung sourceLink de map ra list thiet bi loi dong 108 -> 114
                sourceLink: [...(m.sourceLink || []), dataSocket.sourceLink[0]],
                deviceErrors: deviceErrors,
                type: deviceErrors.length > 0 ? "error" : "DeviceUp"
              };
            }
            /* 20220818 fix */
            return m;
          });
        }
      });

      setNetworkCluster(tempNetworks);
      setMarkerCluster(tempMarkers);
      window.tempSocketData = [];
    }, 3000);
  }
  // 20220818 end


  useEffect(() => {
    if (props.netWorkDevide) {
      // 20220622 Fix start
      setMarkerCluster(props.netWorkDevide.manageUnits);
      setNetworkCluster(props.netWorkDevide.networkLinks);
      let flag = false;
      // setMarkerCluster([
      //   ...(markerCluster || []),
      //   ...(props.netWorkDevide.manageUnits || [])
      // ])
      // setNetworkCluster([
      //   ...(networkCluster || []),
      //   ...(props.netWorkDevide.networkLinks || [])
      // ])
      // 20220622 Fix end
      if (props.locationLevel) {
        if (props.locationLevel.id && props.netWorkDevide) {
          const tempMarkers = props.netWorkDevide.manageUnits && props.netWorkDevide.manageUnits.map(m => {
            if (m.id == props.locationLevel.id) return {
              ...m,
              active: true
            };
            return m;
          });
          setMarkerCluster(tempMarkers);
        }
        if (props.loadLocationLevel) {
          map && map.flyTo(
            [props.locationLevel.lat, props.locationLevel.lng],
            16,
            {
              animate: true,
              duration: 0.5
            }
          )
        }
      }
      // set data netWorkDevide xong moi chay socket dong 497
      if (props.netWorkDevide.manageUnits && props.netWorkDevide.networkLinks) setLoad(false)
    }
  }, [map, props.networkLayer, props.netWorkDevide, props.locationLevel, props.loadLocationLevel])

  return <>
    <div
      style={{
        minHeight: `80vh`,
        height: "100%",
        width: "100%",
        background: '#EEE',
        zIndex: 1,
      }}
      ref={containerMap}
    />
  </>;
}

const mapStateToProps = (state) => {
  return {
  }
}

export default connect(mapStateToProps)(MapComponent)
